# rds/main.tf

resource "aws_db_instance" "mosar_db" {
  allocated_storage                   = var.db_storage
  engine                              = var.db_engine
  engine_version                      = var.db_engine_version
  instance_class                      = var.db_instance_class
  name                                = var.db_name
  username                            = var.db_username
  password                            = var.db_password
  iam_database_authentication_enabled = var.iam_database_authentication_enabled
  db_subnet_group_name                = var.db_subnet_group_name
  vpc_security_group_ids              = var.vpc_security_group_ids
  identifier                          = var.db_identifier
  skip_final_snapshot                 = var.skip_final_snapshot
  multi_az                            = var.multi_az
  maintenance_window                  = var.maintenance_window
  final_snapshot_identifier           = var.final_snapshot_identifier
  deletion_protection                 = var.deletion_protection
  backup_retention_period             = var.backup_retention_period
  enabled_cloudwatch_logs_exports     = var.enabled_cloudwatch_logs_exports

  tags = {
    Name = "mosar_db"
  }
}
