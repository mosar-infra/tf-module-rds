# rds/outputs.tf

output "db_endpoint" {
  value = aws_db_instance.mosar_db.endpoint
}
output "db_hostname" {
  value = split(":", aws_db_instance.mosar_db.endpoint)[0]
}

output "db_arn" {
  value = aws_db_instance.mosar_db.arn
}

output "db" {
  value = aws_db_instance.mosar_db
  sensitive = true
}

output "db_name" {
  value = aws_db_instance.mosar_db.name
}
