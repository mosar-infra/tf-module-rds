# rds/variables.tf

variable "db_storage" {}
variable "db_engine" {}
variable "db_engine_version" {}
variable "db_instance_class" {}
variable "db_name" {}
variable "db_username" {}
variable "db_password" {}
variable "db_subnet_group_name" {}
variable "vpc_security_group_ids" {}
variable "db_identifier" {}
variable "multi_az" {}
variable "maintenance_window" {}
variable "skip_final_snapshot" {}
variable "final_snapshot_identifier" {}
variable "deletion_protection" {}
variable "backup_retention_period" {}
variable "enabled_cloudwatch_logs_exports" {}
variable "iam_database_authentication_enabled" {}

